resource "aws_s3_bucket" "web" {
  bucket = "www.bieber-demo.tk"
  acl    = "public-read"
  policy = file("web_s3_policy.json")

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST","GET"]
    #allowed_origins = ["https://s3-website-test.hashicorp.com"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"] 
    max_age_seconds = 3000
  }
}