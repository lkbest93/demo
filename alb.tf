
resource "aws_lb" "lb" {
  name               = "bieber-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups = [ 
      aws_security_group.allow_http_https_inbound.id, 
      aws_security_group.allow_vpc_inbound.id,
      aws_security_group.allow_outbound.id
  ]
  subnets            = [aws_subnet.public.id,aws_subnet.public_c.id]

  #enable_deletion_protection = true

  tags = {
    Name = "bieber-lb"
  }
}
resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.cert.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.front_end.arn
  }
  depends_on = [
    aws_acm_certificate.cert
  ]
}
resource "aws_lb_listener_rule" "back_proxy" {
  listener_arn = aws_lb_listener.front_end.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.back_end.arn
  }

  condition {
    path_pattern {
      values = ["/api/*"]
    }
  }
}

resource "aws_lb_listener_rule" "jenkins" {
  listener_arn = aws_lb_listener.front_end.arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.jenkins.arn
  }

  condition {
    host_header {
      values = ["jenkins.bieber-demo.tk"]
    }
  }
}
resource "aws_lb_target_group" "jenkins" {
  name     = "jenkins"
  port     = 7000
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id
  health_check {
    path="/login"
  }
} 
resource "aws_lb_target_group_attachment" "jenkins" {
  target_group_arn = aws_lb_target_group.jenkins.arn
  target_id        = aws_instance.jenkins.id
  port             = 7000
}
resource "aws_lb_target_group" "front_end" {
  name     = "tf-example-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id
} 

resource "aws_lb_target_group_attachment" "web" {
  target_group_arn = aws_lb_target_group.front_end.arn
  target_id        = aws_instance.web.id
  port             = 80
}
resource "aws_lb_target_group_attachment" "web_c" {
  target_group_arn = aws_lb_target_group.front_end.arn
  target_id        = aws_instance.web_c.id
  port             = 80
}



resource "aws_lb_target_group" "back_end" {
  name     = "back-end-group"
  port     = 5000
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id
} 
resource "aws_lb_target_group_attachment" "was" {
  target_group_arn = aws_lb_target_group.back_end.arn
  target_id        = aws_instance.was.id
  port             = 5000
}
resource "aws_lb_target_group_attachment" "was_c" {
  target_group_arn = aws_lb_target_group.back_end.arn
  target_id        = aws_instance.was_c.id
  port             = 5000
}

