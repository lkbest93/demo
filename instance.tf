resource "aws_instance" "web" {

    ami = "ami-0ba5cd124d7a79612"         //ubuntu 18.04 lts
    availability_zone = "ap-northeast-2a"
    instance_type = "t2.micro"            
    key_name = aws_key_pair.aws_key_pair.key_name
    subnet_id = aws_subnet.private.id
    security_groups = [
      aws_security_group.allow_vpc_inbound.id,
      aws_security_group.allow_outbound.id,
      aws_security_group.allow_http_https_inbound.id
    ]
    tags = { 
      Name = "bieber-web"
    }
    lifecycle {
      ignore_changes = [
        security_groups,
      ]
    }
}

resource "aws_instance" "web_c" {

    ami = "ami-0ba5cd124d7a79612"         //ubuntu 18.04 lts
    availability_zone = "ap-northeast-2c"
    instance_type = "t2.micro"            
    key_name = aws_key_pair.aws_key_pair.key_name
    subnet_id = aws_subnet.private_c.id
    security_groups = [
      aws_security_group.allow_vpc_inbound.id,
      aws_security_group.allow_outbound.id,
      aws_security_group.allow_http_https_inbound.id
    ]
    tags = { 
      Name = "bieber-web"
    }
    lifecycle {
      ignore_changes = [
        security_groups,
      ]
    }
}


resource "aws_instance" "was" {

    ami = "ami-0ba5cd124d7a79612"         //ubuntu 18.04 lts 
    availability_zone = "ap-northeast-2a"
    instance_type = "t2.micro"            
    key_name = aws_key_pair.aws_key_pair.key_name
    subnet_id = aws_subnet.private.id
    security_groups = [
      aws_security_group.allow_vpc_inbound.id,
      aws_security_group.allow_outbound.id,
      aws_security_group.allow_http_https_inbound.id
    ]
    tags = {
      Name = "bieber-ec2_was_2a"
    }
    lifecycle {
      ignore_changes = [
        security_groups,
      ]
    }
}

resource "aws_instance" "was_c" {

    ami = "ami-0ba5cd124d7a79612"         //ubuntu 18.04 lts 
    availability_zone = "ap-northeast-2c"
    instance_type = "t2.micro"            
    key_name = aws_key_pair.aws_key_pair.key_name
    subnet_id = aws_subnet.private_c.id
    security_groups = [
      aws_security_group.allow_vpc_inbound.id,
      aws_security_group.allow_outbound.id,
      aws_security_group.allow_http_https_inbound.id
    ]
    tags = {
      Name = "bieber-ec2_was_2c"
    }
    lifecycle {
      ignore_changes = [
        security_groups,
      ]
    }
}



resource "aws_instance" "bastion" {

    ami = "ami-0ba5cd124d7a79612"         //ubuntu 18.04 lts 
    availability_zone = "ap-northeast-2a"
    instance_type = "t2.micro"            
    key_name = aws_key_pair.aws_key_pair_bastion.key_name
    subnet_id = aws_subnet.public.id
    security_groups = [
      aws_security_group.allow_my_ssh_inbound.id,
      aws_security_group.allow_vpc_outbound.id
    ]
    tags = {
      Name = "bieber-ec2_bastion_2a"
    }


    lifecycle {
      ignore_changes = [
      security_groups,
      ]
    }

}




resource "aws_db_instance" "db" {
  allocated_storage    = 5
  engine               = "mysql"
  engine_version       = "8.0.20"
  instance_class       = "db.t2.micro"
  name                 = "bb"
  username             = "bb"
  password             = "dlrlqja0085!"
  identifier = "biebier-rds"

  db_subnet_group_name = aws_db_subnet_group.private_group.name
  vpc_security_group_ids = [ aws_security_group.allow_vpc_inbound.id]
  publicly_accessible = false // 프라이빗 엑세스만 허용 
  parameter_group_name = "default.mysql8.0"
  skip_final_snapshot  = true // 종료시 스냅샷 생성 안 함
  multi_az = true  // stanby 생성 옵션
} 

resource "aws_db_subnet_group" "private_group" {
  name       = "private-group"
  subnet_ids = [aws_subnet.private.id,
                aws_subnet.private_c.id]
  tags = {
    Name = "private-group"
  }
}


resource "aws_instance" "jenkins" {

    ami = "ami-0ba5cd124d7a79612"         //ubuntu 18.04 lts 
    availability_zone = "ap-northeast-2a"
    instance_type = "t2.micro"            
    key_name = aws_key_pair.aws_key_pair_bastion.key_name
    subnet_id = aws_subnet.public.id
    security_groups = [
      aws_security_group.allow_my_inbound.id,
      aws_security_group.allow_vpc_inbound.id,
      aws_security_group.allow_outbound.id
    ]
    tags = {
      Name = "bieber-jenkins"
    }


    lifecycle {
      ignore_changes = [
      #security_groups,
      ]
    }

}

 /*
resource "null_resource" "cp_key_to_bastion" {
  triggers = {
      always_run = timestamp()
    }
  provisioner "local-exec" {
    command = <<EOT
    scp -i "/home/bieber/.ssh/bastion"  /home/bieber/.ssh/bieber ubuntu@${aws_instance.bastion.public_ip}:/home/ubuntu/.ssh/bieber
    ssh-keygen -f "/home/bieber/.ssh/known_hosts" -R "${aws_instance.bastion.public_ip}"
    EOT
  }
}

*/