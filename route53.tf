
resource "aws_route53_zone" "zone" {
  name         = "bieber-demo.tk"
}

resource "aws_route53_record" "root" {
  zone_id = aws_route53_zone.zone.zone_id
  name    = "www.bieber-demo.tk"
  type    = "A"
  alias {
    name                   = aws_lb.lb.dns_name
    zone_id                = aws_lb.lb.zone_id
    evaluate_target_health = true
  }
}
resource "aws_route53_record" "jenkins" {
  zone_id = aws_route53_zone.zone.zone_id
  name    = "jenkins.bieber-demo.tk"
  type    = "A"
  alias {
    name                   = aws_lb.lb.dns_name
    zone_id                = aws_lb.lb.zone_id
    evaluate_target_health = true
  }
}



resource "aws_acm_certificate" "cert" {
  domain_name       = "*.bieber-demo.tk"
  validation_method = "DNS"

  tags = {
    Name = "bieber-cert"
  }
  lifecycle {
    create_before_destroy = true
  }
}

  